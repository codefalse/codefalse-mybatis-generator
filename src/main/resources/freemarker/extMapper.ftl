package ${extMapperPackage};

import ${mapperPackage}.${tableName}Mapper

public interface ${tableName}Dao extend ${tableName}Mapper {

}