package com.codefalse.mybatis.generator.constant;

/**
 * @author songxiangfeng
 * Created At 2020-01-14 14:12
 */
public final class SQLiteConstants {

    public static final String SELECT_CONNECTION_NAME = "SELECT `name` FROM `connections`";
    public static final String SELECT_PROJECTS = "SELECT * FROM `projects` where `name` = ? and `database` = ?";
    public static final String INSERT_PROJECT = "INSERT INTO `projects` (`name`, `database`, `path`, `config`) VALUES('%s', '%s', '%s', '%s')";
    public static final String EXISTS_PROJECT = "SELECT count(0) FROM `projects` where `name` = ? and `database` = ? and `path` = ?";
    public static final String UPDATE_PROJECT = "UPDATE `projects` set `config` = ? where `name` = ? and `database` = ? and `path` = ?";


}
