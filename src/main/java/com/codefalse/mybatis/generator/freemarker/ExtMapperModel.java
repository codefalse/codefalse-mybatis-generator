package com.codefalse.mybatis.generator.freemarker;

import lombok.Data;

/**
 * @author songxiangfeng
 * Created At 2021-01-04 19:14
 */
@Data
public class ExtMapperModel {

    private String extMapperPackage;
    private String tableName;

}
