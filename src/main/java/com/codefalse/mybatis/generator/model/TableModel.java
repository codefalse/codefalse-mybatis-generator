package com.codefalse.mybatis.generator.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * @author songxiangfeng
 * Created At 2019-12-19 11:35
 */
public class TableModel {

    private String primaryKey;
    private String table;

    private BooleanProperty enableInsert;
    private BooleanProperty enableDelete;
    private BooleanProperty enableUpdate;
    private BooleanProperty enableSelect;

    private BooleanProperty exampleUpdate;
    private BooleanProperty exampleDelete;
    private BooleanProperty exampleSelect;
    private BooleanProperty exampleCount;

    public TableModel() {
        this.enableInsert = new SimpleBooleanProperty(true);
        this.enableDelete = new SimpleBooleanProperty(true);
        this.enableUpdate = new SimpleBooleanProperty(true);
        this.enableSelect = new SimpleBooleanProperty(true);

        this.exampleUpdate = new SimpleBooleanProperty(false);
        this.exampleDelete = new SimpleBooleanProperty(false);
        this.exampleSelect = new SimpleBooleanProperty(false);
        this.exampleCount = new SimpleBooleanProperty(false);
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public boolean isEnableInsert() {
        return enableInsert.get();
    }

    public BooleanProperty enableInsertProperty() {
        return enableInsert;
    }

    public void setEnableInsert(boolean enableInsert) {
        this.enableInsert.set(enableInsert);
    }

    public boolean isEnableDelete() {
        return enableDelete.get();
    }

    public BooleanProperty enableDeleteProperty() {
        return enableDelete;
    }

    public void setEnableDelete(boolean enableDelete) {
        this.enableDelete.set(enableDelete);
    }

    public boolean isEnableUpdate() {
        return enableUpdate.get();
    }

    public BooleanProperty enableUpdateProperty() {
        return enableUpdate;
    }

    public void setEnableUpdate(boolean enableUpdate) {
        this.enableUpdate.set(enableUpdate);
    }

    public boolean isEnableSelect() {
        return enableSelect.get();
    }

    public BooleanProperty enableSelectProperty() {
        return enableSelect;
    }

    public void setEnableSelect(boolean enableSelect) {
        this.enableSelect.set(enableSelect);
    }

    public boolean isExampleUpdate() {
        return exampleUpdate.get();
    }

    public BooleanProperty exampleUpdateProperty() {
        return exampleUpdate;
    }

    public void setExampleUpdate(boolean exampleUpdate) {
        this.exampleUpdate.set(exampleUpdate);
    }

    public boolean isExampleDelete() {
        return exampleDelete.get();
    }

    public BooleanProperty exampleDeleteProperty() {
        return exampleDelete;
    }

    public void setExampleDelete(boolean exampleDelete) {
        this.exampleDelete.set(exampleDelete);
    }

    public boolean isExampleSelect() {
        return exampleSelect.get();
    }

    public BooleanProperty exampleSelectProperty() {
        return exampleSelect;
    }

    public void setExampleSelect(boolean exampleSelect) {
        this.exampleSelect.set(exampleSelect);
    }

    public boolean isExampleCount() {
        return exampleCount.get();
    }

    public BooleanProperty exampleCountProperty() {
        return exampleCount;
    }

    public void setExampleCount(boolean exampleCount) {
        this.exampleCount.set(exampleCount);
    }
}
