package com.codefalse.mybatis.generator.model;

import lombok.Data;

import java.util.List;

/**
 * @author songxiangfeng
 * Created At 2020-01-07 14:57
 */
@Data
public class CacheModel {

    private List<String> databases;


}
