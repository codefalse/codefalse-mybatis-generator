package com.codefalse.mybatis.generator.model;

import javafx.beans.property.BooleanProperty;
import lombok.Data;

import java.util.List;

/**
 * @author songxiangfeng
 * Created At 2019-12-20 13:24
 */
@Data
public class GenerateConfigModel {

    private String projectPath;
    private String javaPath;
    private String resourcePath;

    private String modelPackage;
    private String mapperPackage;
    private String extMapperPackage;
    private String xmlPackage;
    private String extXmlPackage;
    //更多配置
    private String targetRuntime;
    private Boolean suppressAllComments = false;
    private Boolean supportSerial = false;
    private Boolean supportLombok = false;
    private Boolean annotationMapper = false;
    //表配置
    private List<TableModel> tableModelList;

}
