package com.codefalse.mybatis.generator.model;

import lombok.Data;

/**
 * @author songxiangfeng
 * Created At 2019-12-11 18:16
 */
@Data
public class ConfigModel {

    private String name;
    private String host;
    private Integer port;
    private String database;
    private String username;
    private String password;
    private String args;

    public String packageJDBCUrl() {
        StringBuilder sb = new StringBuilder("jdbc:mysql://");
        sb.append(host).append(":").append(port);
        if (database != null && database != "") {
            sb.append("/").append(database);
        }
        if (args != null && args != "") {
            sb.append("?").append(args);
        }

        return sb.toString();
    }

}
