package com.codefalse.mybatis.generator;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.InnerClass;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.internal.DefaultCommentGenerator;
import org.mybatis.generator.internal.util.StringUtility;

/**
 * @author songxiangfeng
 * Created At 2020-01-21 14:01
 */
public class CodefalseCommentGenerator extends DefaultCommentGenerator {
    @Override
    public void addFieldComment(Field field, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
        String remarks = introspectedColumn.getRemarks();
        if (!StringUtility.stringHasValue(remarks)) {
            return;
        }

        field.addJavaDocLine("/**"); //$NON-NLS-1$
        String[] remarkLines = remarks.split(System.getProperty("line.separator")); //$NON-NLS-1$
        for (String remarkLine : remarkLines) {
            field.addJavaDocLine(" * " + remarkLine); //$NON-NLS-1$
        }

        field.addJavaDocLine(" *"); //$NON-NLS-1$
        field.addJavaDocLine(" */"); //$NON-NLS-1$
    }
}
