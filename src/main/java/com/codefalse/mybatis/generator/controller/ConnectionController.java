package com.codefalse.mybatis.generator.controller;

import com.codefalse.mybatis.generator.model.ConfigModel;
import com.codefalse.mybatis.generator.listener.SubmitConfigNotify;
import com.codefalse.mybatis.generator.util.ConnectionUtils;
import com.codefalse.mybatis.generator.view.ConnectionView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author songxiangfeng
 * Created At 2019-12-10 19:15
 */
@FXMLController
@Slf4j
public class ConnectionController {

    @FXML
    private TextField nameTextField;
    @FXML
    private TextField hostTextField;
    @FXML
    private TextField portTextField;
    @FXML
    private TextField userTextField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField argsTextField;
    @FXML
    private Label errorLabel;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ConnectionView connectionView;
    @Autowired
    private SubmitConfigNotify submitConfigNotify;

    public void onTestConnection() {
        log.debug("test connection");
        errorLabel.setText("");
        ConfigModel configModel = transformToModel();
        if (configModel == null) {
            return;
        }
        String res = ConnectionUtils.testConnection(configModel);
        errorLabel.setText(res);
    }

    public void onSubmitConnection() {
        log.debug("submit connection");
        ConfigModel configModel = transformToModel();
        if (null == configModel) {
            return;
        }
        StringBuilder sb = new StringBuilder("insert into connections values");
        sb.append("(").append("'").append(configModel.getName()).append("'");
        sb.append(",").append("'").append(configModel.getHost()).append("'");
        sb.append(",").append("'").append(configModel.getPort()).append("'");
        sb.append(",").append("'").append(configModel.getUsername()).append("'");
        sb.append(",").append("'").append(configModel.getPassword()).append("'");
        sb.append(")");
        jdbcTemplate.execute(sb.toString());

        submitConfigNotify.insertConfigTreeItem(configModel.getName());
        connectionView.closeWindow();
    }

    private ConfigModel transformToModel() {
        ConfigModel model = new ConfigModel();

        String name = nameTextField.getText();
        if ("".equals(name)) {
            errorLabel.setText("name is not empty.");
            return null;
        }
        String sql = "select count(`name`) from `connections` where `name` = ?";
        Integer count =  jdbcTemplate.queryForObject(sql, new Object[]{name}, Integer.class);
        if (count > 0) {
            errorLabel.setText(name + "is exist.");
            return null;
        }
        String host = hostTextField.getText();
        if ("".equals(host)) {
            host = "127.0.0.1";
        }
        String portStr = portTextField.getText();
        Integer port = 3306;
        if (!"".equals(portStr)) {
            port = Integer.valueOf(portStr);
        }
        String user = userTextField.getText();
        if ("".equals(user)) {
            errorLabel.setText("user is not empty.");
            return null;
        }
        String password = passwordField.getText();
        if ("".equals(password)) {
            errorLabel.setText("password is not empty");
            return null;
        }
        String args = argsTextField.getText();

        model.setName(name);
        model.setHost(host);
        model.setPort(port);
        model.setUsername(user);
        model.setPassword(password);
        model.setArgs(args);
        return model;
    }

}
