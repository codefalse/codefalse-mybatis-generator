package com.codefalse.mybatis.generator.config;

import com.codefalse.mybatis.generator.model.CacheModel;
import com.codefalse.mybatis.generator.model.ConfigModel;
import com.codefalse.mybatis.generator.model.GenerateConfigModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author songxiangfeng
 * Created At 2019-12-11 16:26
 */
@Configuration
public class CodefalseConfig {

    @Bean
    public ConfigModel configModel() {
        return new ConfigModel();
    }

    @Bean
    public Map<String, JdbcTemplate> jdbcTemplateMap() {
        return new HashMap<>();
    }

    @Bean
    public Map<String, GenerateConfigModel> generateConfigMap() {
        return new HashMap<>();
    }

    @Bean
    public Map<String, CacheModel> cacheModelMap() {
        return new HashMap<>();
    }
}
