package com.codefalse.mybatis.generator.listener;

/**
 * @author songxiangfeng
 * Created At 2019-12-11 18:12
 */
public interface SubmitConfigNotify {

    void insertConfigTreeItem(String name);

}
