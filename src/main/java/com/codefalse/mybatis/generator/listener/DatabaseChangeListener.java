package com.codefalse.mybatis.generator.listener;

/**
 * @author songxiangfeng
 * Created At 2019-12-14 15:27
 */
public interface DatabaseChangeListener {

    void currentDatabase(String configName, String database);

}
