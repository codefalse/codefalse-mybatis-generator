package com.codefalse.mybatis.generator.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @author songxiangfeng
 * Created At 2019-12-10 14:32
 */
@FXMLView(value = "/fxml/index.fxml")
public class HomeView extends AbstractFxmlView {

}
