package com.codefalse.mybatis.generator.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;
import javafx.scene.Parent;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * @author songxiangfeng
 * Created At 2019-12-10 16:55
 */
@FXMLView(value = "/fxml/connection.fxml", title = "Add Connection")
public class ConnectionView extends AbstractFxmlView {


    public void closeWindow() {
        Stage stage = (Stage)getView().getScene().getWindow();
        stage.close();
    }

}
