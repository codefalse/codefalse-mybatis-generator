package com.codefalse.mybatis.generator.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;
import javafx.stage.Stage;

/**
 * @author songxiangfeng
 * Created At 2019-12-19 19:29
 */
@FXMLView(value = "/fxml/generateDialog.fxml", title = "通知")
public class GenerateDialogView extends AbstractFxmlView {

    public void closeWindow() {
        Stage stage = (Stage)getView().getScene().getWindow();
        stage.close();
    }

}
