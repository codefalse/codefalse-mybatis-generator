CREATE TABLE if not exists connections (
     name     VARCHAR NOT NULL
         PRIMARY KEY
         UNIQUE,
     host     VARCHAR NOT NULL,
     port     INT     NOT NULL,
     username VARCHAR NOT NULL,
     password VARCHAR NOT NULL
);