CREATE TABLE if not exists projects (
    name   VARCHAR (50)  not null,
    database varchar (50) not null,
    path varchar (50) not null,
    config VARCHAR (500) NOT NULL
);