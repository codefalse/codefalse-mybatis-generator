## codefalse-mybatis-generator
### 快速上手
#### 解压zip，点击startup.sh
#### 新增连接(MENU/File/New)
![add connection](./screenshot/add.png)
#### Home
![home](./screenshot/board.png)
#### 确认弹框
![confirm](./screenshot/confirm.png)
#### 生成日志

![generate-log](./screenshot/generate-log.png)